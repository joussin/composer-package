<?php

namespace MainNamespace\Console\Providers;

use MainNamespace\Console\Commands\CommandComposer;
use MainNamespace\Console\Commands\CommandMakeMigration;
use MainNamespace\Console\Commands\CommandMigrate;
use MainNamespace\Console\Commands\CommandSeeder;

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       // $this->loadMigrationsFrom(__DIR__.'/../../Database/migrations');

        $this->commands([
            CommandSeeder::class,
            CommandMigrate::class,
            CommandMakeMigration::class,
            CommandComposer::class
        ]);

    }


}
