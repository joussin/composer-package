git remote add origin https://gitlab.com/joussin/composer-package.git



Try this :

1) run php artisan app:name YourNamespace
2) rename your app folder to YourNamespace
3) in your bootstrap folder create a file called application.php
4) paste this in there

class Application extends Illuminate\Foundation\Application {

protected $appBasePath = 'app';

public function __construct($basePath = null)
{
$this->registerBaseBindings();

    $this->registerBaseServiceProviders();

    $this->registerCoreContainerAliases();

    if ($basePath) $this->setBasePath($basePath);
}

public function setAppPath($path) {
// store the path in the class only
$this->appBasePath = $path;

    // set the path in the container (using this class's path to reset it)
    return app()->__set('path', $this->path());
}

/**
* Get the path to the application "app" directory.
*
* @return string
  */
  public function path()
  {
  return $this->basePath.DIRECTORY_SEPARATOR.$this->appBasePath;
  }
  }

5) save the file and open app.php
6) and replace your application bootstrap with the following

// load our local application
require __DIR__.'/application.php';

// instaniate our application
$app = new \Application(
realpath(__DIR__.'/../')
);

// set the path to match the namespace
$app->setAppPath('YourNamespace');
7) Save your app.php and that's it

Hope this help you.
