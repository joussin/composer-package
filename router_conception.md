
----------

Application

    class:

        namespace Illuminate\Foundation;
        class Application extends Container implements ApplicationContract, CachesConfiguration, CachesRoutes, HttpKernelInterface
        
        
    Interface

        Interface Application
        namespace Illuminate\Contracts\Foundation;
        
        use Illuminate\Contracts\Container\Container;
        
        interface Application extends Container


    method:

#resolved: array:26 [▶]
#bindings: array:71 [▶]
#methodBindings: []
#instances: array:31 [▶]

    #reboundCallbacks: array:2 [▶]
    #globalBeforeResolvingCallbacks: []
    #globalResolvingCallbacks: []
    #globalAfterResolvingCallbacks: []
    #beforeResolvingCallbacks: []
    #resolvingCallbacks: array:2 [▶]
    #afterResolvingCallbacks: array:4 [▶]
    #basePath: "/Users/waripay/Documents/projet_perso/cms_headless"
    #hasBeenBootstrapped: true
    #booted: false
    #bootingCallbacks: array:2 [▶]
    #bootedCallbacks: array:1 [▶]
    #terminatingCallbacks: array:1 [▶]
    #serviceProviders: & array:29 [▶]





Container

    interface
        
        namespace Illuminate\Contracts\Container;
        
        use Closure;
        use Psr\Container\ContainerInterface;
    
        interface Container extends ContainerInterface
    
    
    
    class
    
    
        namespace Illuminate\Container;
        
        class Container implements ArrayAccess, ContainerContract
        
        



