https://github.com/spatie/regex

composer require spatie/regex

"spatie/regex": "^3.1"



------------------------------------------------------------------------------------------------------------------------
INSTALL:
------------------------------------------------------------------------------------------------------------------------

composer.json

    "require": {
        "php": "^8.0.2",

    "require": {
        "php": "^7.3|^8.0",


    1/ placer le dossier src/ à la racine ./
    2/ spécifier le namespace à la racine de ./src/ dans ./composer.json
    
        "autoload": {
            "psr-4": {
                ...
                "MainNamespace\\": "src/",
                ...


    3/ lancer composer via docker

        docker-compose  -f ./src/docker/docker-compose.yml build
        docker-compose  -f ./src/docker/docker-compose.yml up

        docker-compose  -f ./src/docker/docker-compose-autoload.yml build
        docker-compose  -f ./src/docker/docker-compose-autoload.yml up

    4/ dump autoload:

        php artisan composer:dumpautoload

------------------------------------------------------------------------

Provider:

    config/app.php
        'providers' => [
            ...
            MainServiceProvider

            
------------------------------------------------------------------------

command:

php artisan about


custom:

php artisan composer:dumpautoload


------------------------------------------------------------------------

.env:
    
    DB_CONNECTION=mysql
    DB_DATABASE=cms_headless
    DB_HOST=192.168.0.21
    DB_PORT=3094
    DB_USERNAME=root
    DB_PASSWORD=wg2bAQhd36aJ


------------------------------------------------------------------------

Database:


    create migration:

        create table:

           php artisan  database:make  'table_name' --create

        update table:

            php artisan  database:make  'table_name' --table

    Tables migration: 
        
        all:
            php artisan database:migrate

        by filename:
            php artisan database:migrate --name=2023_01_04_213214_create_template_table.php
            php artisan database:migrate --name=2023_01_04_213241_create_page_table.php

    Seeder:
        all:
            php artisan database:seeder

        by filename:
            php artisan database:seeder --seeder=PageSeeder
            php artisan database:seeder --seeder=TemplateSeeder


    rollback:

        php artisan migrate:rollback

        php artisan migrate:rollback --step=1

        php artisan migrate:reset

------------------------------------------------------------------------

server:


    php artisan serve







