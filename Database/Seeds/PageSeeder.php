<?php

namespace MainNamespace\Database\Seeds;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $template_input_data = [
            'title' => "title content",
            'author' => "stephane",
            'content' => "content data",
        ];

        $pageCartegrise = [
            'version' => 1,
            'alias' => "carte-grise",
            'url' => "http://127.0.0.1:8000/carte-grise",
            'path' => "/carte-grise",
            'staticDocName' => 'carte-grise.html',
            'staticUrl' => 'http://127.0.0.1:8000/carte-grise.html',
            'queryParam' => null,
            'rewriteRule' => null,

            'template_id' => 1,
            'template_input_data' => json_encode($template_input_data),


            'metadata' => null,
            'statut' => "ONLINE",
            'active_start_at' => "2023-01-01",
            'active_end_at' => "2023-01-05",
        ];

        DB::table('page')->insert($pageCartegrise);



        $template_input_data = [
            'title' => "title content",
            'author' => "stephane",
            'content' => "content data",
        ];

        $page2 = [
            'version' => 1,
            'alias' => "bo-test",
            'url' => "http://127.0.0.1:8000/bo-test",
            'path' => "/bo-test",
            'staticDocName' => 'bo-test.html',
            'staticUrl' => 'http://127.0.0.1:8000/bo-test.html',
            'queryParam' => null,
            'rewriteRule' => null,

            'template_id' => 2,
            'template_input_data' => json_encode($template_input_data),


            'metadata' => null,
            'statut' => "ONLINE",
            'active_start_at' => "2023-01-01",
            'active_end_at' => "2023-01-05",
        ];

        DB::table('page')->insert($page2);




    }
}
