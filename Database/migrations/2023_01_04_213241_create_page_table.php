<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page', function (Blueprint $table) {
            $table->id();
            $table->integer('version');
            $table->string('alias');
            $table->string('url');
            $table->string('path');
            $table->string('staticDocName')->nullable();
            $table->string('staticUrl')->nullable();
            $table->string('queryParam')->nullable();
            $table->string('rewriteRule')->nullable();

            $table->integer('template_id');
            $table->json('template_input_data')->nullable();

            $table->json('metadata')->nullable();
            $table->enum('statut', ['ONLINE', 'OFFLINE']);

            $table->date('active_start_at');
            $table->date('active_end_at');


            // UNIQUE
            $table->unique(['version', 'id']);
            $table->unique(['version', 'alias']);
            $table->unique(['version', 'url']);
            $table->unique(['version', 'path']);
            $table->unique(['version', 'staticDocName']);
            $table->unique(['version', 'staticUrl']);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(\Illuminate\Support\Facades\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

//            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page');
    }
};
