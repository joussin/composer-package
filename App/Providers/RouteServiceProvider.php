<?php

namespace MainNamespace\App\Providers;

use MainNamespace\App\Http\Middleware\RouteMiddleware;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{


    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     *
     * @return void
     */
    public function boot()
    {
        // advanced.router
        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('route.middleware', RouteMiddleware::class);


        $custom = base_path('src/App/routes/routes.php');
        $api = base_path('src/App/routes/api.php');
        $web = base_path('src/App/routes/web.php');


        $this->loadRoutesFrom($custom);

        $this->routes(function () use ($web, $api) {

            Route::middleware('api')
                ->prefix('api')
                ->group($api);

            Route::middleware('web')
                ->group($web);
        });
    }



}
