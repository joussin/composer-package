<?php

namespace MainNamespace\App\Providers;

use MainNamespace\App\Services\MainService;
use MainNamespace\App\Services\FacadeService;
use MainNamespace\Console\Providers\ConsoleServiceProvider;
use Illuminate\Support\ServiceProvider;

class MainServiceProvider extends ServiceProvider
{

    protected const PREFIX = 'MY_LARAVEL_APP';

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [MainService::class, FacadeService::class];

    }



    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->mergeConfigFrom(__DIR__.'/../config/app.php', self::PREFIX);
//        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'hashtagcms');
//        $this->loadViewsFrom(__DIR__.'/../resources/views', 'hashtagcms');


        // Register the service the package provides.
        $this->app->singleton('customfacadeaccessor', function ($app) {
            return new FacadeService();
        });

        $this->app->singleton("MainService", function() {
            return new MainService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(AppServiceProvider::class);
        $this->app->register(ConsoleServiceProvider::class);
    }


}
