<?php

namespace MainNamespace\App\Http\Controllers;

use MainNamespace\App\Facades\HelperFacade;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function home()
    {
        return "home";
    }


    public function test()
    {
        $config = config('laravel_module');

        $mainservice = app()->MainService->info();

        $facadeHelper = HelperFacade::helper();

        $ar = [
            $config, $mainservice, $facadeHelper
        ];

        return $ar;
    }

    public function any()
    {
        return "any";
    }

}
