<?php


namespace MainNamespace\App\Http\Middleware;

use Illuminate\Http\Request;


class RouteMiddleware
{
    public function handle(Request $request, \Closure $next)
    {

        return $next($request);
    }
}
